import React from "react";
import {Button, Paper, TextField, Typography} from '@material-ui/core';
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
        container: {
            display: 'flex', height: '100vh', backgroundColor: '#eeeeee'
        },
        loginBase: {
            width: '300px', height: '200px', margin: 'auto', padding: '10px'
        },
        field: {width: '80%'},
        loginButton: {margin: '15px'}
    }
);
//自作するコンポーネントは基本的に先頭大文字！！
const LoginForm = () => {
    const classes =useStyles();
    return (
        <div className={classes.container}>
            {/*Paperはほかのコンポーネントを乗せる土台コンポーネント*/}
            <Paper className={classes.loginBase}>
                {/*Typographyはテキストコンテンツ用のコンポーネント。variantの設定により大きさを指定する。*/}
                <Typography>ログインしてください。</Typography>
                {/*TextFieldは入力欄とラベルがセットになったコンポーネント。Labelは画面表記されるラベルの内容*/}
                <TextField className={classes.field} label='Name'/>
                <TextField className={classes.field} label='Password'/>
                {/*Buttonコンポーネントを利用　variantは外見の指定、colorは色の指定*/}
                <Button className={classes.loginButton}
                    variant='contained' color='primary'>
                    ログイン
                </Button>
            </Paper>
        </div>
    );
};
export default LoginForm;